# Projet Android

Voici la page git de notre projet universitaire en développement mobile : TrapQuizz

## Description
TrapQuizz est un jeu de questions pièges, où les réponses de sont pas toujours évidentes !
Le jeu contient actuellement une pile de 10 niveaux. Le jeu fonctionne par "stack" de niveaux; C'est à dire qu'il faut compléter tous les niveaux d'une pile pour débloquer la suivante. Et lorsque l'on perd sur l'un des niveaux d'une pile, on doit recommencer la pile depuis le début.

## Installation du projet
Il y a 3 méthodes pour installer et tester notre application :

- Avec fichier zip
    * Télécharger le dépot en zip
    * Décompresser le fichier 
    * Ouvrir Android Studio
    * Cliquer sur "Open an existing Android Studio project"
    * Sélectionner le dossier décompressé  <br /> <br />
- Avec git clone
    * Exécuter la commande "git clone https://gitlab.com/trapquizz/android.git"
    * Ouvrir Android Studio
    * Cliquer sur "Open an existing Android Studio project"
    * Sélectionner le dossier cloné <br /> <br />
- Avec Android Studio
    * Ouvrir Android Studio
    * Cliquer sur "Check out project from Version Control"
    * Sélectionner "git"
    * Entrer dans le champ URL :  https://gitlab.com/trapquizz/android.git
    * Cliquer sur "Clone"

## Télécharger l'APK
Vous pouvez télécharger notre application et l'installer [ici](https://gitlab.com/trapquizz/android/blob/dev/TrapQuizz.apk)

## JavaDoc
[JavaDoc](https://gitlab.com/trapquizz/android/tree/dev/JavaDoc)

## Resources externes
- Pour les icones : https://www.flaticon.com
- Pour la musique du jeu : https://downloads.khinsider.com

## Présentation
- Rapport en Latex : 
    * Overleaf : https://www.overleaf.com/read/bswhjvvfjmdt
    * Dossier : [Rapport](https://gitlab.com/trapquizz/android/tree/dev/Latex/Rapport) <br /> <br />
- Diaporama en Latex (Beamer) :
    * Overleaf : https://www.overleaf.com/read/sgqnnkgwvjyq
    * Dossier : [Slides](https://gitlab.com/trapquizz/android/tree/dev/Latex/Slides)

## Crédits
Projet réalisé par Barret Hervé et Ramanitra Willy, etudiant en L3 Informatique à l'Université de la Réunion. (2020)