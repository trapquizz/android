package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level3Activity extends AppCompatActivity {
    private EditText input;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();
    private String[] Answers;
    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        input = findViewById(R.id.textInput);
        Answers = new String[4];
        Answers[0] = getResources().getString(R.string.q1c1);
        Answers[1] = getResources().getString(R.string.q1c2);
        Answers[2] = getResources().getString(R.string.q1c3);
        Answers[3] = getResources().getString(R.string.q1c4);

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        captureKeyListener();
        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /** Test le saisie de l'utilisateur
     *
     *
     * @param v
     */
    public void sendInput (View v){
        boolean find = false;
        int i;
        for (i =0; Answers.length> i; i++){
            if (input.getText().toString().toLowerCase().equals(Answers[i].toLowerCase())){
                i = Answers.length;
                find = true;
                showPopup();
            }
        }
        if (!find){
            Life.lessOne();
            countLife.setText(String.valueOf(Life.getLife()));
            if (Life.getLife() == 0){
                showPopupLose();
            }
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level4Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    public void captureKeyListener() {
        input.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sendInput(getWindow().getDecorView().findViewById(android.R.id.content));
                    return true;
                }
                return false;
            }
        });
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
