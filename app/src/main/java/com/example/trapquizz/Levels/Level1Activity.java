package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level1Activity extends AppCompatActivity {

    private TextView countLife;
    private RadioButton c5;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);

        if(Life.getLife()==0){
            showPopupLose();
        }

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        c5 = findViewById(R.id.q1c5);

        String user = pref.getString("User", "UnknowUser");
        String question = getResources().getString(R.string.q1);
        question = String.format(question, user);
        TextView title = findViewById(R.id.question2);
        title.setText(question);

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView textViewlevel = findViewById(R.id.level);

        //Animation
        textViewlevel.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * Vérifie si l'utilisateur a choisi la bonne réponse
     * @param v
     */
    public void choice (View v) {
        if (c5.isChecked()){
            showPopupNext();
        } else {
            Life.lessOne();
            countLife.setText(String.valueOf(Life.getLife()));
            if (Life.getLife() == 0){
                showPopupLose();
            }
        }
    }

    /**
     * Le pop up qui s'affiche quand on gagne
     * fait appel à la classe PopupLevel
     */
    public void showPopupNext(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(Level1Activity.this);
        p.setC(Level2Activity.class);
        p.build();
    }


    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(Level1Activity.this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private Music mServ;
    private ServiceConnection Scon = new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }

}
