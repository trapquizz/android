package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level10Activity extends AppCompatActivity{

    private TextView mTextField, code;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();
    private Button send;
    private EditText textInput;
    private String answer, q10;
    private SharedPreferences pref;

    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level10);
        mTextField = findViewById(R.id.question6);
        code = findViewById(R.id.code);
        answer = getResources().getString(R.string.code);
        q10 = getResources().getString(R.string.preq10);
        textInput = findViewById(R.id.textInput);
        send = findViewById(R.id.sendInput);
        textInput.setVisibility(View.INVISIBLE);
        send.setVisibility(View.INVISIBLE);
        pref = getApplicationContext().getSharedPreferences("pref",0);

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }

        CountDownTimer countDownTimer = new CountDownTimer(5 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                mTextField.setText(q10+"\n"+millisUntilFinished / 1000);
            }

            public void onFinish() {
                code.setVisibility(View.INVISIBLE);
                send.setVisibility(View.VISIBLE);
                textInput.setVisibility(View.VISIBLE);
                mTextField.setText(R.string.q10);
            }
        };
        countDownTimer.start();
        captureKeyListener();
    }

    public void sendInput(View view) {
        if((textInput.getText().toString().toLowerCase()).equals(answer)) {
            pref = getApplicationContext().getSharedPreferences("pref",0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("Stack1", "done");
            editor.apply();
            showPopup();
        }
        else {
            Life.lessOne();
            countLife.setText(String.valueOf(Life.getLife()));
            if(Life.getLife()==0){
                showPopupLose();
            }
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(MainActivity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    public void captureKeyListener() {
        textInput.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                sendInput(getWindow().getDecorView().findViewById(android.R.id.content));
                Toast.makeText(Level10Activity.this,textInput.getText(), Toast.LENGTH_LONG).show();
                return true;
            }
            return false;
            }
        });
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
