package com.example.trapquizz.Levels;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level8Activity extends AppCompatActivity {

    private int count;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();
    private Button buttonAfficher;
    private ImageView imageViewDuck;
    private boolean firstTouch = false;
    private long time;

    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level8);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        count = 0;

        buttonAfficher = findViewById(R.id.btnMove);
        imageViewDuck = findViewById(R.id.imageView2);

        buttonAfficher.setVisibility(buttonAfficher.INVISIBLE);
        buttonAfficher.setClickable(false);

        imageViewDuck.setVisibility(imageViewDuck.INVISIBLE);
        imageViewDuck.setClickable(false);
        
        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            doBindService();
            Intent music = new Intent();
            music.setClass(this, Music.class);
            startService(music);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == event.ACTION_DOWN){
            if(firstTouch && (System.currentTimeMillis() - time) <= 300) {
                firstTouch = false;
                buttonAfficher.setVisibility(buttonAfficher.VISIBLE);
                buttonAfficher.setClickable(true);
            }else {
                firstTouch = true;
                time = System.currentTimeMillis();
                return super.onTouchEvent(event);
            }
        }
        return super.onTouchEvent(event);
    }

    public void afficher(View view) {
        Toast.makeText(this,getResources().getString(R.string.toast81),Toast.LENGTH_LONG).show();
        imageViewDuck.setVisibility(imageViewDuck.VISIBLE);
        imageViewDuck.setClickable(true);
    }

    public void touchDuck1(View view) {
        count +=1;
        if(count==1){
            Toast.makeText(this,getResources().getString(R.string.toast82),Toast.LENGTH_LONG).show();
        }else if (count>1){
            Life.lessOne();
            countLife.setText(String.valueOf(Life.getLife()));
            Toast.makeText(this,"Yoo!!"+getResources().getString(R.string.toast83),Toast.LENGTH_LONG).show();
            if (Life.getLife()==0){
                showPopupLose();
            }
        }
    }

    public void touchDuck2(View view) {
        showPopup();
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level9Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
