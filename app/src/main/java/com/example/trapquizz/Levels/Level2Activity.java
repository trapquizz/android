package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.AproposActivity;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level2Activity extends AppCompatActivity {
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();
    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        String question = getResources().getString(R.string.q2);
        String button = getResources().getString(R.string.button2);
        TextView q2 = findViewById(R.id.question2);
        singleTextView(q2, button, question);

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * Permet de créer la question avec un mot cliquable
     * @param textView
     * @param button
     * @param question
     */
    private void singleTextView(TextView textView, String button, String question) {

        SpannableStringBuilder spanText = new SpannableStringBuilder();
        spanText.append(question+" ");
        spanText.append(button);
        spanText.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                theButton(widget);
                widget.invalidate(); // permet de supprimer l'effet surlignement
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.jaune));
                textPaint.setUnderlineText(false);      // this remove the underline
            }
        }, spanText.length() - button.length(), spanText.length(), 0);


        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spanText, TextView.BufferType.SPANNABLE);

    }


    /**
     * onClick du bouton caché dans la question
     * @param v
     */
    public void theButton (View v){
        showPopup();
    }

    /**
     * onClick du faux bouton, et enlève une vie au joueur. Retourne au menu s'il n'a plus de vie
     * @param v
     */
    public void BtnFake (View v){
        Life.lessOne();
        countLife.setText(String.valueOf(Life.getLife()));
        if (Life.getLife() == 0){
            showPopupLose();
        }

    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level3Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
