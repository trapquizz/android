package com.example.trapquizz.Levels;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Display;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level9Activity extends AppCompatActivity {

    private int animation, width, height;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();

    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level9);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);
        animation = 0;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * OnClick du faux bouton ouvrant un alert dialog
     * @param view
     */
    public void fakeSkip(View view){
        switch (animation){
            case 0:
                view.animate().translationX(width/3);
                view.animate().translationY(height/3);
                break;
            case 1:
                view.animate().translationX(-width/4);
                view.animate().translationY(-height/3);
                break;
            case 2:
                view.animate().translationX(width/2);
                view.animate().translationY(-height/3);
                break;
            case 3:
                view.animate().translationX(-width/5);
                view.animate().translationY(height/5);
                break;
            case 4:
                view.animate().translationX(-width/3);
                view.animate().translationY(height/3);
                break;
            case 5:
                view.animate().translationX(width/3);
                view.animate().translationY(height/4);
                break;
            case 6:
                view.animate().translationX(0);
                view.animate().translationY(0);
                break;
            default:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage(R.string.messAlert);
                alertDialogBuilder.setPositiveButton(R.string.messButtonYes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                Toast.makeText(Level9Activity.this,R.string.toastYes,Toast.LENGTH_LONG).show();
                                actionLife();
                            }
                        });

                alertDialogBuilder.setNegativeButton(R.string.messButtonNo,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(Level9Activity.this,R.string.toastNo,Toast.LENGTH_LONG).show();
                        actionLife();
                        //finish();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;
        }
        animation += 1;
    }

    /**
     * OnClick du titre "niveau 09" permettant de passer au niveau suivant
     * @param v
     */
    public void printSoluce (View v){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.messSoluce);
        alertDialogBuilder.setPositiveButton(R.string.messButtonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        showPopup();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Fonction de gestion de la vie du joueur
     */
    public void actionLife() {
        Life.lessOne();
        countLife.setText(String.valueOf(Life.getLife()));
        if (Life.getLife() <= 0) {
            showPopupLose();
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level10Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
