package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level7Activity extends AppCompatActivity {

    private int order = 1;
    private String[] orderAnswer, orderSent;
    private Button[] buttonList;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();

    private TextView countLife;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level7);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        Button c1 = findViewById(R.id.c1);
        Button c2 = findViewById(R.id.c2);
        Button c3 = findViewById(R.id.c3);
        Button c4 = findViewById(R.id.c4);

        orderAnswer = new String[4];
        orderSent = new String[4];
        buttonList = new Button[4];
        buttonList[0] = c1;
        buttonList[1] = c2;
        buttonList[2] = c3;
        buttonList[3] = c4;
        orderAnswer[0] = c1.getText().toString().toLowerCase();
        orderAnswer[1] = c3.getText().toString().toLowerCase();
        orderAnswer[2] = c2.getText().toString().toLowerCase();
        orderAnswer[3] = c4.getText().toString().toLowerCase();
        
        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    public void order(View v){
        if (order <= 4){
            Button button = (Button) v;
            orderSent[order-1] = button.getText().toString().toLowerCase();
            button.setText(order + " " + button.getText());
            order += 1;
        }
    }

    public void sendOrder(View v){
        for (int i = 0; i<orderSent.length; i++){
            if (!orderSent[i].equals(orderAnswer[i])){
                for (Button button : buttonList) {
                    String num = button.getText().toString().toLowerCase().substring(0, 1);
                    String text = num + " " + orderSent[i];
                    if (button.getText().toString().toLowerCase().equals(text)) {
                        button.setTextAppearance(R.style.ButtonsError);
                    }
                }
                Life.lessOne();
                countLife.setText(String.valueOf(Life.getLife()));
                if(Life.getLife()<0){
                    showPopupLose();
                }
            } else {
                showPopup();
            }
        }
    }

    public void resetOrder (View v){
        String text;
        order = 1;
        for (int i = 0; i<buttonList.length; i++){
            switch(i){
                case 0: text = getResources().getString(R.string.q7c1);
                    break;
                case 1: text = getResources().getString(R.string.q7c2);
                    break;
                case 2: text = getResources().getString(R.string.q7c3);
                    break;
                case 3: text = getResources().getString(R.string.q7c4);
                    break;
                default:
                    text = "error";
            }

            buttonList[i].setText(text);
            buttonList[i].setTextAppearance(R.style.Buttons);
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level8Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
