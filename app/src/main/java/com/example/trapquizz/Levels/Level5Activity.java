package com.example.trapquizz.Levels;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.Classes.PowerReceiver;
import com.example.trapquizz.R;

import java.util.Locale;

public class Level5Activity extends AppCompatActivity {

    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();

    private float xCoOrdinate, yCoOrdinate;

    private TextView countLife;

    private int count;

    PowerReceiver powerReceiver = new PowerReceiver();

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level5);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);

        count = 0;


        //create an IntentFilter object that matches battery change action
        IntentFilter filter=new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        //register broadcast receiver to receive a sticky intent
        Intent intent=this.registerReceiver(null, filter);
        //get battery status from the intent
        assert intent != null;
        int status=intent.getIntExtra(BatteryManager.EXTRA_STATUS,-1);
        if(status==BatteryManager.BATTERY_STATUS_CHARGING) {
            showPopup();
        }

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        ImageView imageView4 = findViewById(R.id.imageView4);
        ImageView imageView5 = findViewById(R.id.imageView5);
        ImageView imageView6 = findViewById(R.id.imageView6);
        ImageView[] images = new ImageView[3];
        images[0] = imageView4;
        images[1] = imageView5;
        images[2] = imageView6;

        for (ImageView image : images) {
            image.setOnTouchListener(new View.OnTouchListener() {
                /**
                 * Permet de déplaçer une image et execute la fonction touch()
                 */
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getActionMasked()) {
                        case MotionEvent.ACTION_DOWN:
                            xCoOrdinate = v.getX() - event.getRawX();
                            yCoOrdinate = v.getY() - event.getRawY();
                            break;
                        case MotionEvent.ACTION_MOVE:
                            v.animate().x(event.getRawX() + xCoOrdinate).y(event.getRawY() + yCoOrdinate).setDuration(0).start();
                            break;
                        case MotionEvent.ACTION_UP:
                            touch();
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });
        }

        TextView level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * onStart de la page qui vérifie si le chargeur est branché ou non
     */
    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(Intent.ACTION_POWER_CONNECTED);
        registerReceiver(powerReceiver, filter);
    }

    /**
     * Enlève une vie au joueur s'il touche une image, retourne au menu s'il n'a plus de vie
     */
    public void touch() {
        Life.lessOne();
        countLife.setText(String.valueOf(Life.getLife()));
        if (Life.getLife() == 0) {
            showPopupLose();
        }
    }

    public void Pass(View view) {
        if(Life.getLife()==1){
            count+=2;
            Toast.makeText(this,getResources().getString(R.string.toast55),Toast.LENGTH_LONG).show();
        }
        else {
            count +=1;
            if(count==1){
                Toast.makeText(this,getResources().getString(R.string.toast53),Toast.LENGTH_LONG).show();
            }
            if(count==2){
                Toast.makeText(this,getResources().getString(R.string.toast54),Toast.LENGTH_LONG).show();
            }
            if(count==3){
                Life.lessOne();
                countLife.setText(String.valueOf(Life.getLife()));
                startActivity(new Intent(this, Level1Activity.class));
                finish();
            }
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level6Activity.class);
        p.build();
    }

    public void showPopupLose(){
        PopupLose pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    public void loadLanguage(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}