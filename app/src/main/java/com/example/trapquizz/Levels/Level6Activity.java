package com.example.trapquizz.Levels;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Classes.PopupLose;
import com.example.trapquizz.R;

public class Level6Activity extends AppCompatActivity {

    private EditText editText;
    private String answer;
    private boolean find;
    private com.example.trapquizz.Classes.Life Life = com.example.trapquizz.Classes.Life.getInstance();

    private TextView level;
    private TextView countLife;
    private PopupLevel p;
    private PopupLose pl;
    private SharedPreferences pref;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level6);

        pref = getApplicationContext().getSharedPreferences("pref",0);

        answer = "kenji kawakami";

        countLife = findViewById(R.id.countLife);
        countLife.setText(String.valueOf(Life.getLife()));

        editText = findViewById(R.id.textInput);

        level = findViewById(R.id.level);
        //Animation
        level.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        captureKeyListener();

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * Test la saisie du joueur, enlève une vie si la saisie n'est pas bonne et retourne au menu s'il n'a plus de vie
     * @param view
     */
    public void sendInput(View view) {
        find = false;
        if((editText.getText().toString().toLowerCase()).equals(answer)) {
            showPopup();
        }
        else {
            Life.lessOne();
            countLife.setText(String.valueOf(Life.getLife()));
            if(Life.getLife()==0){
                showPopupLose();
            }
        }
    }

    public void showPopup(){
        p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level7Activity.class);
        p.build();
    }

    public void showPopupLose(){
        pl = new PopupLose(this);
        pl.setText(getResources().getString(R.string.lose));
        pl.setContext(this);
        pl.setC(MainActivity.class);
        pl.build();
    }

    public void captureKeyListener() {
        editText.setOnKeyListener(new View.OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sendInput(getWindow().getDecorView().findViewById(android.R.id.content));
                    return true;
                }
                return false;
            }
        });
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private boolean mIsBound = false;
    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
