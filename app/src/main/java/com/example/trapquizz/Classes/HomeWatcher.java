package com.example.trapquizz.Classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Classe permettant d'intéragir avec les boutons "Home" et "Back" des apparareils Android.
 * Dans notre cas, il mettra en pause la musique du jeu lorsque l'on est plus sur l'application
 */
public class HomeWatcher {

    private Context context;
    private IntentFilter intentFilter;
    private OnHomePressedListener onHomePressedListener;
    private InnerRecevier innerRecevier;

    public HomeWatcher(Context context) {
        this.context = context;
        intentFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
    }

    public void setOnHomePressedListener(OnHomePressedListener listener) {
        onHomePressedListener = listener;
        innerRecevier = new InnerRecevier();
    }

    public void startWatch() {
        if (innerRecevier != null) {
            context.registerReceiver(innerRecevier, intentFilter);
        }
    }

    class InnerRecevier extends BroadcastReceiver {
        final String SYSTEM_DIALOG_REASON_KEY = "reason";
        final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
        final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
                if (reason != null) {
                    if (onHomePressedListener != null) {
                        if (reason.equals(SYSTEM_DIALOG_REASON_HOME_KEY)) {
                            onHomePressedListener.onHomePressed();
                        } else if (reason.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
                            onHomePressedListener.onHomeLongPressed();
                        }
                    }
                }
            }
        }
    }

    public interface OnHomePressedListener {
        void onHomePressed();

        void onHomeLongPressed();
    }
}
