package com.example.trapquizz.Classes;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import com.example.trapquizz.R;


/**
 * La classe Music est un Service permettant d'être actif sur toute l'application, et joue la musique du jeu continuellement
 */
public class Music extends Service {
    private final IBinder binder = new ServiceBinder();
    MediaPlayer player;
    private int length = 0;

    /**
     * Constructeur de la classe Music
     */
    public Music() {
    }

    public class ServiceBinder extends Binder {
        public Music getService() {
            return Music.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /**
     * Lance la musique
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (player != null) {
            player.start();
        }
        return START_NOT_STICKY;
    }

    /**
     * Eteint la musique
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            try {
                player.stop();
                player.release();
            } finally {
                player = null;
            }
        }
    }

    /**
     * OnCreate de la musique, définie la musique à jouer, le volume et si elle doit se jouer en boucle
     */
    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.backgroundsound);
        if (player != null){
            player.setLooping(true); // Set looping
            player.setVolume(100, 100);
        }
    }

    /**
     * Remet la musique du jeu en play
     */
    public void resume(){
        if (player != null) {
            if (!player.isPlaying()) {
                player.seekTo(length);
                player.start();
            }
        }
    }

    /**
     * Met en pause la musique
     */
    public void pause(){
        if (player != null) {
            if (player.isPlaying()) {
                player.pause();
                length = player.getCurrentPosition();
            }
        }
    }

}
