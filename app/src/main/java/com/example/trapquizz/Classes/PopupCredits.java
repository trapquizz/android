package com.example.trapquizz.Classes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.Menus.OptionsActivity;
import com.example.trapquizz.R;

/**
 * Classe gérant le pop-up des crédits
 */
public class PopupCredits extends Activity {

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_popupcredits);

        TextView textViewCredit = findViewById(R.id.textCredit);
        Button buttonOk = findViewById(R.id.buttonOk);

        //Animations
        textViewCredit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        buttonOk.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_inv));

        //Les dimensions du pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

    }

    public void ok(View view) {
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, OptionsActivity.class));
        finish();
    }
}
