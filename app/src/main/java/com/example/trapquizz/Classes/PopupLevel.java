package com.example.trapquizz.Classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Menus.MainActivity;
import com.example.trapquizz.R;

import java.util.Objects;

/**
 * Classe permettant de créer des pop-ups (victoire, niveaux bloqués...)
 */
public class PopupLevel extends Dialog {

    private String text;
    private TextView textView;
    private Button button;
    private Context context;
    private Class c;

    public PopupLevel(Activity activity){
        super(activity, R.style.Theme_AppCompat_Dialog);
        setContentView(R.layout.activity_popuplevel);

        Objects.requireNonNull(this.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        this.text = getText();
        this.textView = findViewById(R.id.textView3);
        this.button = findViewById(R.id.button);
        this.context = activity;
        this.c = getC();

        button.startAnimation(AnimationUtils.loadAnimation(context, R.anim.shake_inv));
    }

    public Button getButton(){return button;}

    public String getText() { return text; }
    public void setText(String text) { this.text = text; }

    public void setContext(Context context) { this.context = context; }

    public Class getC() { return c; }
    public void setC(Class c) { this.c = c; }

    public void build(){

        show();
        textView.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, c));
                ((Activity) context).finish();
            }
        });
    }

    public void builLocked(){
        show();
        textView.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
    }

    public void buildMain(){
        show();
        textView.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)context.getApplicationContext()).finish();
            }
        });
    }
}
