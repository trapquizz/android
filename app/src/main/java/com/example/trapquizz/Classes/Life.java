package com.example.trapquizz.Classes;

import android.widget.TextView;

/**
 * Life est la classe définissant la vie du joueur
 */
public class Life{
    private int life;

    private static final Life Life = new Life();

    /**
     * Constructeur de la classe Life
     */
    private Life(){

    }

    /**
     * Méthode permettant de récupérer l'unique objet Life.
     * Cette classe est codé en singleton
     * @return Life est l'objet définissant la vie du joueur
     */
    public static final Life getInstance(){
        return Life;
    }

    /**
     * Retroune la vie du joueur
     * @return life
     */
    public int getLife() {
        return life;
    }

    /**
     * Modifie la vie du joueur
     * @param life est le nombre de vie à donner
     */
    public void setLife(int life) {
        this.life = life;
    }

    /**
     * Enlève une vie au joueur
     */
    public void lessOne(){
        this.life = life - 1;
    }

    public void showLife(int l, TextView t1, TextView t2, TextView t3){
        if(l==2){ t1.setVisibility(t1.INVISIBLE);}

        if (l==1){
            t1.setVisibility(t1.INVISIBLE);
            t2.setVisibility(t2.INVISIBLE);}

        if (l==0){
            t1.setVisibility(t1.INVISIBLE);
            t2.setVisibility(t2.INVISIBLE);
            t3.setVisibility(t3.INVISIBLE);}
    }

}
