package com.example.trapquizz.Classes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.BatteryManager;
import android.widget.Toast;

/**
 * La classe PowerReceiver est un BroadcastReceiver et est appelé lorsque le téléphone est sur secteur
 */
public class PowerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())) {
            boolean noConnectivity = intent.getBooleanExtra(
                    Intent.ACTION_POWER_DISCONNECTED, false
            );
            if (noConnectivity) {
                Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();
                Intent i = new Intent();
                i.setClassName("com.example.trapquizz", "com.example.trapquizz.Levels.Level6Activity");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
    }
}
