package com.example.trapquizz.Classes;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;


/**
 * Classe gérant la langue du jeu
 */
public class Language extends Application {

    /**
     * Cette méthode permet de garder la langue du jeu lorsque l'appareil change d'orientation
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        SharedPreferences pref = getApplicationContext().getSharedPreferences("pref", 0);
        setLocale(getApplicationContext(),pref.getString("Lang", "fr"));
        Log.d("configchanged", getResources().getConfiguration().locale.getLanguage());
    }

    /**
     * Méthode permettant de changer la langue du jeu
     * @param context
     * @param lang est un String pour définir la langue ("fr" ou "en")
     */
    public static void setLocale(Context context, String lang) {
        Resources res = context.getResources();
        Locale locale = new Locale(lang);
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration config = res.getConfiguration();
        Locale.setDefault(locale);
        config.locale = locale;
        res.updateConfiguration(config, dm);
        SharedPreferences pref = context.getSharedPreferences("pref", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("Lang", lang);
        editor.apply();
    }

}

