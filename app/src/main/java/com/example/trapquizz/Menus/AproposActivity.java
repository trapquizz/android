package com.example.trapquizz.Menus;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.R;

public class AproposActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);

        //Button & Text animations utilise la R @anim/shake
        TextView textView = findViewById(R.id.titleApropos);
        textView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

    }

    /**
     * Transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, MainActivity.class));
    }

}
