package com.example.trapquizz.Menus;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Classes.Language;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupCredits;
import com.example.trapquizz.R;

import java.util.Locale;

public class OptionsActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Switch switchSound;

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        pref = getApplicationContext().getSharedPreferences("pref",0);
        TextView textView = findViewById(R.id.textApropos);
        switchSound = findViewById(R.id.switchSon);
        Switch switchLang = findViewById(R.id.switchLangue);
        Button buttonCredits = findViewById(R.id.Credit);

        //Etablie le switch sur on ou off en fonction des paramètres sauvegardés
        if ("fr".equals(pref.getString("Lang","fr"))){
            switchLang.setChecked(false);
        } else {
            switchLang.setChecked(true);
        }

        //Etablie le switch sur on ou off en fonction des paramètres sauvegardés
        if (pref.getString("Sound","on").equals("on")){
            switchSound.setChecked(true);
        } else {
            switchSound.setChecked(false);
        }

        //Animation
        textView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        switchLang.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_inv));
        switchSound.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        buttonCredits.startAnimation(AnimationUtils.loadAnimation(this,R.anim.shake_inv));

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * Onclick du bouton Crédit ouvrant un pop up
     * @param view
     */
    public void open(View view){
        startActivity(new Intent(OptionsActivity.this, PopupCredits.class));
    }

    /**
     * OnClick du switch permettant de changer la langue du jeu
     * @param v
     *
     */
    public void SelectLanguage (View v){
        if ("fr".equals(pref.getString("Lang","fr"))){
            //setLocale("en");
            Language.setLocale(getApplicationContext(),"en");
        } else {
            //setLocale("fr");
            Language.setLocale(getApplicationContext(),"fr");
        }

        Intent i=new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    /**
     * Enregistre la langue du jeu et redirige vers le menu pour mettre à jour la langue
     * @param lang est la langue que l'on veut paramétrer
     */
    public void setLocale(String lang){
        Locale locale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        Locale.setDefault(locale);
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        pref = getApplicationContext().getSharedPreferences("pref",0);
        editor = pref.edit();
        editor.putString("Lang",lang);
        editor.apply();
        Intent i=new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    /**
     * OnClick du switch du son, permettant d'activer la musique de fond ou non
     * @param v
     */
    public void setMusic(View v){
        String sound;
        if (switchSound.isChecked()){
            sound = "on";
        } else {
            sound = "off";
            mServ.pause();
        }
        editor = pref.edit();
        editor.putString("Sound",sound);
        editor.apply();
        Intent i=new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private Music mServ;
    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
