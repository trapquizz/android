package com.example.trapquizz.Menus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.trapquizz.Classes.Language;
import com.example.trapquizz.Classes.Life;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Levels.Level1Activity;
import com.example.trapquizz.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LevelsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private SharedPreferences pref;
    private Life Life = com.example.trapquizz.Classes.Life.getInstance();

    /**
     * Permet de retourner au Menu principal si l'on appuie sur le bouton retour
     * Ajoute une transition Animation on back button
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Animatoo.animateSlideRight(this);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);

        pref = getApplicationContext().getSharedPreferences("pref",0);

        Life.setLife(3);

        TextView textViewNiveaux = findViewById(R.id.textViewNiveaux);

        ListView listview = findViewById(R.id.levelList);
        listview.setOnItemClickListener(this);

        // Initializing a new String Array
        String[] levels = new String[] {
                getResources().getString(R.string.stack1),
                getResources().getString(R.string.stack2),
                getResources().getString(R.string.stack3),
                getResources().getString(R.string.stack4),
                getResources().getString(R.string.stack5)
        };

        // Create a List from String Array elements
        final List<String> levels_list = new ArrayList<String>(Arrays.asList(levels));

        // Create an ArrayAdapter from List
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                (this, R.layout.list, levels_list){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the current item from ListView
                View view = super.getView(position,convertView,parent);
                return view;
            }
        };

        // DataBind ListView with items from ArrayAdapter
        listview.setAdapter(arrayAdapter);

        //Animations
        textViewNiveaux.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        Language.setLocale(getApplicationContext(),pref.getString("Lang", "fr"));
        Log.d("configchangedLEVELS", getResources().getConfiguration().locale.getLanguage());

        if (pref.getString("Sound", "on").equals("on")) {
            MusicB();
        }
    }

    /**
     * Cette fonction permet de définir les actions à réaliser en fonction de l'item sélectionné par l'utilisateur
     * Ici, elle permet de rediriger l'utilisateur vers le niveau correspondant, et indique si le niveau est bloqué ou pas
     * @param l est la liste parcouru
     * @param v est la view
     * @param position est la position de l'item dans la liste
     * @param id est l'id de l'item
     */
    public void onItemClick(AdapterView<?> l, View v, int position, long id) {
        Class redirection = MainActivity.class;
        boolean lock = true;
        switch (position){
            case 0:
                redirection = Level1Activity.class;
                lock = false;
                break;
            case 1:
                if ("done".equals(pref.getString("Stack1", null))){
                    redirection = Level1Activity.class;
                    lock = false;
                }
                break;
            case 2:
                if ("done".equals(pref.getString("Stack2", null))){
                    redirection = Level1Activity.class;
                    lock = false;
                }
                break;
            case 3:
                if ("done".equals(pref.getString("Stack3", null))){
                    redirection = Level1Activity.class;
                    lock = false;
                }
                break;
            case 4:
                if ("done".equals(pref.getString("Stack4", null))){
                    redirection = Level1Activity.class;
                    lock = false;
                }
                break;
            default:
                redirection = MainActivity.class;
                break;
        }
        Log.i("HelloListView", "You clicked Item: " + id + " at position:" + position);
        // Then you start a new Activity via Intent
        if (!lock){
            Intent intent = new Intent();
            intent.setClass(this, redirection);
            intent.putExtra("position", position);
            intent.putExtra("id", id);
            startActivity(intent);
            finish();
        }else {
            showPopup();
        }
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.levelLocked));
        p.setContext(this);
        p.setC(LevelsActivity.class);
        p.builLocked();
    }


    //MUSIC SECTION

    public void MusicB(){
        doBindService();
        Intent music = new Intent();
        music.setClass(this, Music.class);
        startService(music);
    }

    private Music mServ;
    private ServiceConnection Scon = new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            mServ = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            mServ = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mServ != null) {
            mServ.resume();
        }
    }
}
