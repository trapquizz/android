package com.example.trapquizz.Menus;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.trapquizz.Classes.HomeWatcher;
import com.example.trapquizz.Classes.Language;
import com.example.trapquizz.Classes.Music;
import com.example.trapquizz.Classes.PopupLevel;
import com.example.trapquizz.Levels.Level5Activity;
import com.example.trapquizz.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private Button BtnPlay;
    private Button BtnAbout;
    private SharedPreferences.Editor editor;
    private String user;

    private boolean mIsBound = false;
    private Music music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getApplicationContext().getSharedPreferences("pref",0);

        if (pref.getString("Sound", "on").equals("on")){
            //BIND Music Service
            doBindService();
            Intent musicServ = new Intent();
            musicServ.setClass(this, Music.class);
            startService(musicServ);

            //Start HomeWatcher
            HomeWatcher mHomeWatcher = new HomeWatcher(this);
            mHomeWatcher.setOnHomePressedListener(new HomeWatcher.OnHomePressedListener() {
                @Override
                public void onHomePressed() {
                    if (music != null) {
                        music.pause();
                    }
                }
                @Override
                public void onHomeLongPressed() {

                    if (music != null) {
                        music.pause();
                    }
                }
            });
            mHomeWatcher.startWatch();
        }

        TextView textView = findViewById(R.id.textApropos);
        BtnPlay = findViewById(R.id.button);
        BtnAbout = findViewById(R.id.button3);
        Button btnOption = findViewById(R.id.button2);
        Button btnExit = findViewById(R.id.button4);

        //Animations
        textView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        BtnPlay.startAnimation(AnimationUtils.loadAnimation(this,R.anim.shake_inv));
        btnOption.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
        BtnAbout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake_inv));
        btnExit.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));

        // Virer ces 3 lignes pour éviter de demander à chaque ouverture d'application
        /*editor = pref.edit();
        editor.putString("User","UnknowUser");
        editor.apply();*/

        loadLanguage(pref.getString("Lang","fr"));

        if (pref.getString("User", "UnknowUser").equals("UnknowUser")){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.name);

            // Set up the input
            final EditText input = new EditText(this);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    user = input.getText().toString();
                    editor = pref.edit();
                    editor.putString("User",user);
                    editor.apply();
                    Toast.makeText(MainActivity.this,"Menfou !",Toast.LENGTH_LONG).show();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        } else {
            pref.getString("User", "UnknowUser");
        }

        Language.setLocale(getApplicationContext(),pref.getString("Lang", "fr"));
        Log.d("configchangedMAIN", getResources().getConfiguration().locale.getLanguage());
    }

    public void showPopup(){
        PopupLevel p = new PopupLevel(this);
        p.setText(getResources().getString(R.string.win));
        p.setContext(this);
        p.setC(Level5Activity.class);
        p.buildMain();
    }

    private ServiceConnection Scon =new ServiceConnection(){

        public void onServiceConnected(ComponentName name, IBinder
                binder) {
            music = ((Music.ServiceBinder)binder).getService();
        }

        public void onServiceDisconnected(ComponentName name) {
            music = null;
        }
    };

    void doBindService(){
        bindService(new Intent(this,Music.class),
                Scon,Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    void doUnbindService()
    {
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }

    /**
     * onResume du menu permettant de remettre la musique du jeu
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (music != null) {
            music.resume();
        }
    }

    /**
     * onPause du menu permettant de mettre en pause la musique du jeu
     */
    @Override
    protected void onPause() {
        super.onPause();

        //Detect idle screen
        PowerManager pm = (PowerManager)
                getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = false;
        if (pm != null) {
            isScreenOn = pm.isScreenOn();
        }

        if (!isScreenOn) {
            if (music != null) {
                music.pause();
            }
        }
    }

    /**
     * onDestroy du menu permettant de reset la musique du jeu
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        //UNBIND music service
        doUnbindService();
        Intent music = new Intent();
        music.setClass(this,Music.class);
        stopService(music);

    }

    /**
     * Charge le langage précédemment sauvegardé
     * @param lang
     */
    public void loadLanguage(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        BtnPlay.setText(R.string.play);
        BtnAbout.setText(R.string.about);
    }

    /**
     * OnClick du bouton Jouer, qui redirige vers la sélection de niveau
     * @param v
     */
    public void Play(View v){
        startActivity(new Intent(this, LevelsActivity.class));
    }

    /**
     * OnClick du bouton option, qui redirige vers les options du jeu
     * @param v
     */
    public void Options(View v){
        startActivity(new Intent(this, OptionsActivity.class));
    }

    /**
     * OnClick du bouton A Propos, qui redirige vers la description du jeu
     * @param v
     */
    public void About(View v){
        startActivity(new Intent(this, AproposActivity.class));
    }

    /**
     * OnClick du bouton quitter, qui quitte le jeu
     * @param view
     */
    public void Exit(View view) {
        super.onDestroy();
        finishAndRemoveTask();
        System.exit(0);
    }

}
